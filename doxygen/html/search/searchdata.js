var indexSectionsWithContent =
{
  0: "ceilmnpst",
  1: "e",
  2: "mst",
  3: "mst",
  4: "cilnpst",
  5: "t",
  6: "ps",
  7: "ms"
};

var indexSectionNames =
{
  0: "all",
  1: "classes",
  2: "files",
  3: "functions",
  4: "variables",
  5: "typedefs",
  6: "enums",
  7: "defines"
};

var indexSectionLabels =
{
  0: "All",
  1: "Data Structures",
  2: "Files",
  3: "Functions",
  4: "Variables",
  5: "Typedefs",
  6: "Enumerations",
  7: "Macros"
};

