/**
  * @file my_user_threads.h
  * @date 20.10.2016
  * @author Dawid Sabat
  * @version 1.0
  * @brief Library that stores functions that enables user threads.
  * @details
  */
//=================================================================================================
#ifndef MY_USER_THREADS_H_
#define MY_USER_THREADS_H_
//=====================================INCLUDES====================================================
#include "thread_scheduler.h"
//=================================================================================================
//=====================================DEFINES=====================================================

//=================================================================================================
//=====================================TYPEDEFS====================================================

//=================================================================================================
//=====================================HEADERS=====================================================
/**
 * @brief Creates and starts given thread.
 * @param thread: pointer to given task.
 */
int mythreads_start(void* thread);
/**
 * @brief Ends task work from that task.
 */
int mythread_exit(void);
/**
 * @brief Kills task from outside that task.
 * @param tid: task name.
 */
int mythread_kill(void* tid);
//=================================================================================================
#endif /* MY_USER_THREADS_H_ */
//=====================================EOF=========================================================
