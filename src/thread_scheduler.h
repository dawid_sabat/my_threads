/**
  * @file thread_scheduler.h
  * @date 18.10.2016
  * @author Dawid Sabat
  * @version 1.0
  * @brief Library that handles thread scheduler algorithm.
  * @details
  */
//=================================================================================================
#ifndef THREAD_SCHEDULER_H_
#define THREAD_SCHEDULER_H_
//=====================================INCLUDES====================================================
#include <malloc.h>
#include <signal.h>
#include <sys/time.h>
#include <stdio.h>
#include <stdlib.h>
#include <ucontext.h>
//=================================================================================================
//=====================================DEFINES=====================================================
/** @brief One second given in milliseconds. */
#define SCHEDULER_ONE_SECOND_IN_MICROSECONDS			1000000
/** @brief Interval for each task. */
#define SCHEDULER_TASKS_INTERVAL_US						10
/** @brief Memory reserved for each task. */
#define MEM 											64000
//=================================================================================================
//=====================================TYPEDEFS====================================================
/** @brief Structure with two way cyclic list. */
typedef struct element
{
	struct element *prev_list_element_ptr; ///< Pointer to the previous element on the list.
	struct element *next_list_element_ptr; ///< Pointer to the next element on the list.
	ucontext_t* ctx_name; ///< Value of context for given task.
	void* task_name; ///< Task name (function address).
}two_way_thread_list_element;
//=================================================================================================
/** @brief Enum with scheduler status. */
typedef enum
{
	SCHEDULER_NOT_CONFIGURED = 0,
	SCHEDULER_STARTED,
	SCHEDULER_STOPPED,
	SCHEDULER_ERR
}scheduler_timer_state;
//=================================================================================================
/** @brief Enum with list status. */
typedef enum
{
	SCHEDULER_LIST_NOT_INITALIZED = 0,
	SCHEDULER_LIST_EMPTY,
	SCHEDULER_ONE_ELEMENT_ON_LIST,
	SCHEDULER_LIST_POPULATED,
}scheduler_list_state;
//=================================================================================================
/** @brief Enum with status of removing task from the list. */
typedef enum
{
	SCHEDULER_REMOVING_TASK_FAIL = 0,
	SCHEDULER_REMOVING_TASK_SUCCESS,
	SCHEDULER_LAST_TASK_REMOVED,
}scheduler_romoving_state;
//=================================================================================================
/** @brief Enum with status if given task is on the list. */
typedef enum
{
	SCHEDULER_ELEMENT_ON_LIST = 0,
	SCHEDULER_NO_SPECIFIC_ELEMENT,
}presence_on_list;
//=================================================================================================
//=====================================HEADRES=====================================================
/**
 * @brief Initialization of the first empty element on the list by allocation some space in memory.
 */
void Scheduler_InitTaskList(void);
//=================================================================================================
/**
 * @brief Adding new task and its context to the list.
 * @param new_task_address: pointer to task that is added.
 * @param ctx_name: pointer to context which is connected with added task.
 * @retval SCHEDULER_ONE_ELEMENT_ON_LIST: when it is the first task added to the list.
 * @retval SCHEDULER_LIST_POPULATED: when it is the second and more task added to the list.
 */
scheduler_list_state Scheduler_AddToList(void* new_task_address, ucontext_t* ctx_name);
//=================================================================================================
/**
 * @brief Returns status of population on the tasks list.
 * @details This function enables to determine if the tasks list is empty, there is only one element, or there is more.
 * @retval SCHEDULER_ONE_ELEMENT_ON_LIST: when it is the first task added to the list.
 * @retval SCHEDULER_LIST_POPULATED: when it is the second and more task added to the list.
 * @retval SCHEDULER_LIST_EMPTY: when the tasks list is initalized but it is empty.
 * @retval SCHEDULER_LIST_NOT_INITALIZED: when the tasks list was not initalized.
 */
scheduler_list_state Scheduler_GetListPopulationState(void);
//=================================================================================================
/**
 * @brief Enables to set new state of population on the tasks list.
 * @param new_state: new population state.
 */
void Scheduler_SetListPopulationState(scheduler_list_state new_state);
//=================================================================================================
/**
 * @brief Adds first task to the initalized tasks list.
 * @param new_task_address: pointer to task that is added.
 * @param ctx_name: pointer to context which is connected with added task.
 * @param list_ptr: pointer to the tasks list.
 */
void Scheduler_AddFirstElementToList(void* new_task_address, ucontext_t* ctx_name,
		two_way_thread_list_element *list_ptr);
//=================================================================================================
/**
 * @brief Finds tasks list tail.
 * @details This function enables to find last element on the tasks list (its tail). List's tail is an element
 * 			which pointer to the next element points to list's first element (its head).
 * @param list_ptr: pointer to the tasks list.
 * @retval Pointer to list's tail.
 */
two_way_thread_list_element* Scheduler_FindLastElementOnList(two_way_thread_list_element *list_ptr);
//=================================================================================================
/**
 * @brief Adds new element to list's end (tail).
 * @param new_task_address: pointer to task that is added.
 * @param ctx_name: pointer to context which is connected with added task.
 * @param list_ptr: pointer to the tasks list.
 */
void Scheduler_AddElementToListTail(void* new_task_address, ucontext_t* ctx_name,
		two_way_thread_list_element *list_ptr);
//=================================================================================================
/**
 * @brief Removes specified task from tasks list.
 * @param task_address: addres of removed task.
 * @retval SCHEDULER_REMOVING_TASK_FAIL: when no specified task was found.
 * @retval SCHEDULER_REMOVING_TASK_SUCCESS: when specified task was removed successfully.
 */
scheduler_romoving_state Scheduler_RemoveFromList(void* task_address);
//=================================================================================================
/**
 * @brief Finds is specified element is on the tasks list.
 * @param list_ptr: pointer to the tasks list.
 * @param task_address: pointer to specified task.
 * @retval Pointer to place on the list where specified element is located.
 * @retval NULL: when there was no specified element on the tasks list.
 */
two_way_thread_list_element* Scheduler_FindIfElementIsOnList(two_way_thread_list_element* list_ptr,
		void* task_address);
//=================================================================================================
/**
 * @brief Deletes current task from the tasks list.
 * @retval SCHEDULER_REMOVING_TASK_FAIL: when no specified task was found.
 * @retval SCHEDULER_REMOVING_TASK_SUCCESS: when specified task was removed successfully.
 */
scheduler_romoving_state Scheduler_DeleteCurrentListElement(void);
//=================================================================================================
/**
 * @brief Lists all tasks addresses on the list.
 */
void Scheduler_ListWriteDown(void);
//=================================================================================================
/**
 * @brief Goes to the next element on the tasks list.
 */
void Scheduler_NextElementOnList(void);
//=================================================================================================
/**
 * @brief Initializes scheduler timer with given interval.
 * @param timer_interval_us: interval for scheduler timer in microseconds.
 */
void Scheduler_TimerInit(int timer_interval_us);
//=================================================================================================
/**
 * @brief Starts scheduler timer with given interval.
 * @param timer_interval_us: interval for scheduler timer in microseconds.
 */
void Scheduler_TimerStart(int timer_interval_us);
//=================================================================================================
/**
 * @brief Starts scheduler timer with default settings.
 */
void Scheduler_TimerStartDefault();
//=================================================================================================
/**
 * @brief Sets scheduler timer default settings.
 * @param timer_interval_us: interval for scheduler timer in microseconds.
 */
void Scheduler_TimerSetDafaultInterval(int timer_interval_us);
//=================================================================================================
/**
 * @brief Stops scheduler timer.
 */
void Scheduler_TimerStop(void);
//=================================================================================================
/**
 * @brief Enables to change scheduler timer status.
 * @param new_status: new scheduler timer status.
 */
void Scheduler_TimerChangeStatus(scheduler_timer_state new_status);
//=================================================================================================
/**
 * @brief Gets current scheduler timer status.
 * @retval SCHEDULER_NOT_CONFIGURED
 * @retval SCHEDULER_STARTED
 * @retval SCHEDULER_STOPPED
 * @retval SCHEDULER_ERR
 */
scheduler_timer_state Scheduler_TimerGetStatus(void);
//=================================================================================================
/**
 * @brief Handles interrupt from scheduler timer.
 * @param sig: signal.
 */
void Scheduler_TimerHandler(int sig);
//=================================================================================================
/**
 * @brief Initializes context for given task.
 * @param ctx_name: pointer to context parameters.
 * @param task_name: pointer to task address.
 */
void Scheduler_Init_Context(ucontext_t* ctx_name, void* task_name);
//=================================================================================================
/**
 * @brief Releases given context.
 * @param ctx_name: pointer to context parameters.
 */
void Scheduler_Free_Context(ucontext_t* ctx_name);
/**
 * @brief Gets pointer to the current list element.
 * @retval Pointer to the current task.
 */
two_way_thread_list_element* Scheduler_GetCurrentListElement(void);
//=================================================================================================
#endif /* THREAD_SCHEDULER_H_ */
//=====================================EOF=========================================================
