//=================================================================================================
/**
  * @file my_user_threads.c
  * @date 20.10.2016
  * @author Dawid Sabat
  * @version 1.0
  * @brief Library that stores functions that enables user threads.
  * @details
  */
//=================================================================================================
//=====================================INCLUDES====================================================
#include "my_user_threads.h"
//=================================================================================================
//=====================================VARIABLES===================================================

//=================================================================================================
//=====================================FUNCTIONS===================================================
int mythreads_start(void* thread)
{
	int exit_status;
	ucontext_t* user_thread_ctx;
	user_thread_ctx =(ucontext_t*) malloc (sizeof(ucontext_t));
	Scheduler_Init_Context(user_thread_ctx, (void*)thread);
	exit_status = (int)(Scheduler_AddToList((void*)thread, user_thread_ctx));

	return exit_status;
}
//=================================================================================================
int mythread_exit(void)
{
	int exit_status;
	Scheduler_TimerStop();

	exit_status = (int)(Scheduler_DeleteCurrentListElement());
	Scheduler_ListWriteDown();

	Scheduler_TimerStartDefault();

	setcontext(Scheduler_GetCurrentListElement()->ctx_name);

	return exit_status;
}
//=================================================================================================
int mythread_kill(void* tid)
{
	int exit_status;

	Scheduler_TimerStop();
	exit_status = (int)Scheduler_RemoveFromList(tid);
	Scheduler_TimerStartDefault();

	return exit_status;
}
//=====================================EOF=========================================================
