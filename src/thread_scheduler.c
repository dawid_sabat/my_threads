/**
  * @file thread_scheduler.c
  * @date 18.10.2016
  * @author Dawid Sabat
  * @version 1.0
  * @brief Library that handles thread scheduler algorithm.
  * @details
  */
//=================================================================================================
//=====================================INCLUDES====================================================
#include "thread_scheduler.h"
//=================================================================================================
//=====================================VARIABLES===================================================
/** @brief Pointer to the list with tasks. */
two_way_thread_list_element *list_element;
/** @brief Variable with status of tasks list population. */
scheduler_list_state list_population_state = SCHEDULER_LIST_EMPTY;
/** @brief Pointer to the head of tasks list. */
struct element *list_head;
/** @brief Variable with status of the scheduler. */
scheduler_timer_state scheduler_state = SCHEDULER_LIST_NOT_INITALIZED;
/** @brief Variable with interval given for each task. */
int scheduler_timer_default_interval_us = 0;
//=================================================================================================
//=====================================FUNCTIONS===================================================
void Scheduler_InitTaskList(void)
{
	list_element =(two_way_thread_list_element*) malloc (sizeof(two_way_thread_list_element));
	list_element->next_list_element_ptr = NULL;
	list_element->prev_list_element_ptr = NULL;
	list_element->task_name = NULL;
	list_head = NULL;
	Scheduler_SetListPopulationState(SCHEDULER_LIST_EMPTY);
}
//=================================================================================================
scheduler_list_state Scheduler_AddToList(void* new_task_address, ucontext_t* ctx_name)
{
	two_way_thread_list_element *list_ptr;
	two_way_thread_list_element *last_list_element_ptr;

	list_ptr = list_element;

	switch(Scheduler_GetListPopulationState())
	{
		case SCHEDULER_LIST_EMPTY:
		{
			Scheduler_AddFirstElementToList((void*)new_task_address, ctx_name, list_ptr);
			Scheduler_SetListPopulationState(SCHEDULER_ONE_ELEMENT_ON_LIST);
			break;
		}
		case SCHEDULER_ONE_ELEMENT_ON_LIST:
		{
			last_list_element_ptr = Scheduler_FindLastElementOnList(list_ptr);
			Scheduler_AddElementToListTail((void*)new_task_address, ctx_name, last_list_element_ptr);
			Scheduler_SetListPopulationState(SCHEDULER_LIST_POPULATED);
			break;
		}
		case SCHEDULER_LIST_POPULATED:
		{
			last_list_element_ptr = Scheduler_FindLastElementOnList(list_ptr);
			Scheduler_AddElementToListTail((void*)new_task_address, ctx_name, last_list_element_ptr);
			break;
		}
		default:
		{
			break;
		}
	}
	return Scheduler_GetListPopulationState();
}
//=================================================================================================
scheduler_list_state Scheduler_GetListPopulationState(void)
{
	return list_population_state;
}
//=================================================================================================
void Scheduler_SetListPopulationState(scheduler_list_state new_state)
{
	list_population_state = new_state;
}
//=================================================================================================
void Scheduler_AddFirstElementToList(void* new_task_address, ucontext_t* ctx_name,
		two_way_thread_list_element *list_ptr)
{
	list_ptr->next_list_element_ptr = list_ptr;
	list_ptr->prev_list_element_ptr = list_ptr;

	list_head = list_ptr;

	list_ptr->task_name = new_task_address;
	list_ptr->ctx_name = ctx_name;
}
//=================================================================================================
two_way_thread_list_element* Scheduler_FindLastElementOnList(two_way_thread_list_element *list_ptr)
{
	while(list_ptr->next_list_element_ptr != list_head)
	{
		list_ptr = list_ptr->next_list_element_ptr;
	}

	return list_ptr;
}
//=================================================================================================
void Scheduler_AddElementToListTail(void* new_task_address, ucontext_t* ctx_name,
		two_way_thread_list_element *list_ptr)
{
	two_way_thread_list_element *new_list_element;

	new_list_element =(two_way_thread_list_element*) malloc (sizeof(two_way_thread_list_element));

	new_list_element->prev_list_element_ptr = list_ptr;
	new_list_element->next_list_element_ptr = list_head;

	new_list_element->task_name = new_task_address;
	new_list_element->ctx_name = ctx_name;

	list_ptr->next_list_element_ptr = new_list_element;
	list_head->prev_list_element_ptr = new_list_element;
}
//=================================================================================================
scheduler_romoving_state Scheduler_RemoveFromList(void* task_address)
{
	two_way_thread_list_element *list_ptr;
	two_way_thread_list_element *deleted_ptr;
	two_way_thread_list_element *current_list_element;

	list_ptr = list_element;
	current_list_element = list_element;
	deleted_ptr = list_ptr;

	deleted_ptr = Scheduler_FindIfElementIsOnList(list_ptr, (void*)task_address);

	if(deleted_ptr == NULL)
	{
		printf("Task ID %x does not exist\r\n", ((int*)(task_address)));
		return SCHEDULER_REMOVING_TASK_FAIL;
	}

	if(list_ptr->next_list_element_ptr == list_ptr->prev_list_element_ptr)
	{
		if(list_ptr->task_name == task_address)
		{
			list_ptr->next_list_element_ptr = NULL;
			list_ptr->prev_list_element_ptr = NULL;
			list_head = NULL;
			list_ptr->task_name = 0;
			Scheduler_SetListPopulationState(SCHEDULER_LIST_EMPTY);
			return SCHEDULER_LAST_TASK_REMOVED;
		}
	}
	else
	{
		if(deleted_ptr == list_head)
		{
			list_head = deleted_ptr->next_list_element_ptr;
			list_element = list_head;
		}

		if(deleted_ptr == current_list_element)
		{
			list_element = current_list_element->next_list_element_ptr;
		}

		free (deleted_ptr);
		return SCHEDULER_REMOVING_TASK_SUCCESS;
	}
	return SCHEDULER_REMOVING_TASK_FAIL;
}
//=================================================================================================
two_way_thread_list_element* Scheduler_FindIfElementIsOnList(two_way_thread_list_element* list_ptr,
		void* task_address)
{
	while(list_ptr->next_list_element_ptr != list_head)
	{
		if(list_ptr->task_name == task_address)
		{

			return list_ptr;
		}
		list_ptr = list_ptr->next_list_element_ptr;
	}

	return NULL;
}
//=================================================================================================
scheduler_romoving_state Scheduler_DeleteCurrentListElement(void)
{
	 return Scheduler_RemoveFromList((void*)list_element->task_name);
}
//=================================================================================================
void Scheduler_ListWriteDown(void)
{
	two_way_thread_list_element *list_ptr;

	list_ptr = list_head;

	do
	{
		printf ("%x\n", (int)list_ptr->task_name);
		list_ptr = list_ptr->next_list_element_ptr;
	}
	while(list_ptr->next_list_element_ptr != list_head);
	printf ("%x\n", (int)(list_ptr->task_name));
	list_ptr = list_ptr->next_list_element_ptr;
}
//=================================================================================================
void Scheduler_NextElementOnList(void)
{
	list_element = list_element ->next_list_element_ptr;
}
//=================================================================================================
void Scheduler_TimerInit(int timer_interval_us)
{
	struct sigaction act;
	struct sigaction oact;

	act.sa_handler = Scheduler_TimerHandler;
	sigemptyset(&act.sa_mask);
	act.sa_flags = 0;
	sigaction(SIGPROF, &act, &oact);

	Scheduler_TimerSetDafaultInterval(timer_interval_us);
	Scheduler_TimerStartDefault();
}
//=================================================================================================
void Scheduler_TimerStart(int timer_interval_us)
{
	struct itimerval it;

	int timer_interval_s = timer_interval_us % SCHEDULER_ONE_SECOND_IN_MICROSECONDS;
	timer_interval_us -= timer_interval_us * SCHEDULER_ONE_SECOND_IN_MICROSECONDS;

	// Start itimer
	it.it_interval.tv_sec = timer_interval_s;
	it.it_interval.tv_usec = timer_interval_us;
	it.it_value.tv_sec = timer_interval_s;
	it.it_value.tv_usec = timer_interval_us;
	setitimer(ITIMER_PROF, &it, NULL);
	Scheduler_TimerChangeStatus(SCHEDULER_STARTED);
}
//=================================================================================================
void Scheduler_TimerStartDefault(void)
{
	struct itimerval it;

	int timer_interval_s = scheduler_timer_default_interval_us / SCHEDULER_ONE_SECOND_IN_MICROSECONDS;
	int timer_interval_us = scheduler_timer_default_interval_us % SCHEDULER_ONE_SECOND_IN_MICROSECONDS;

	it.it_interval.tv_sec = timer_interval_s;
	it.it_interval.tv_usec = timer_interval_us;
	it.it_value.tv_sec = timer_interval_s;
	it.it_value.tv_usec = timer_interval_us;
	setitimer(ITIMER_PROF, &it, NULL);
	Scheduler_TimerChangeStatus(SCHEDULER_STARTED);
}
//=================================================================================================
void Scheduler_TimerSetDafaultInterval(int timer_interval_us)
{
	scheduler_timer_default_interval_us = timer_interval_us;
}
//=================================================================================================
void Scheduler_TimerStop(void)
{
	struct itimerval it;

	it.it_interval.tv_sec = 0;
	it.it_interval.tv_usec = 0;
	it.it_value.tv_sec = 0;
	it.it_value.tv_usec = 0;
	setitimer(ITIMER_PROF, &it, NULL);
	Scheduler_TimerChangeStatus(SCHEDULER_STOPPED);
}
//=================================================================================================
void Scheduler_TimerChangeStatus(scheduler_timer_state new_status)
{
	scheduler_state = new_status;
}
//=================================================================================================
scheduler_timer_state Scheduler_TimerGetStatus(void)
{
	return scheduler_state;
}
//=================================================================================================
void Scheduler_TimerHandler(int sig)
{
	ucontext_t* current_ctx;
	ucontext_t* next_ctx;
	puts("Hello from timer handler");

	current_ctx = list_element->ctx_name;
	getcontext(current_ctx);
	Scheduler_NextElementOnList();
	next_ctx = list_element->ctx_name;

	swapcontext(current_ctx, next_ctx);
}
//=================================================================================================
void Scheduler_Init_Context(ucontext_t* ctx_name, void* task_name)
{
	getcontext(ctx_name);
	ctx_name->uc_link = 0;
	ctx_name->uc_stack.ss_sp = malloc(MEM);
	ctx_name->uc_stack.ss_size = MEM;
	ctx_name->uc_stack.ss_flags = 0;
	makecontext(ctx_name, (void*)task_name, 0);
}
//=================================================================================================
void Scheduler_Free_Context(ucontext_t* ctx_name)
{
	ctx_name->uc_link = 0;
	free(ctx_name->uc_stack.ss_sp);
	ctx_name->uc_stack.ss_size = 0;
	ctx_name->uc_stack.ss_flags = 0;
}
//=================================================================================================
two_way_thread_list_element* Scheduler_GetCurrentListElement(void)
{
	return &(*list_element);
}
//=====================================EOF=========================================================
