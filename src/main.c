//=================================================================================================
/**
  * @file main.c
  * @author Dawid Sabat
  * @brief Main file.
  */
//=================================================================================================
//=====================================INCLUDES====================================================
#include <stdio.h>
#include <stdlib.h>
#include <ucontext.h>
#include <malloc.h>


#include "thread_scheduler.h"
#include "my_user_threads.h"
#include "semaphores.h"
//=================================================================================================
//=====================================DEFINES=====================================================
//=================================================================================================
//=====================================VARIABLES===================================================
int important_calculation = 0; ///< Variable used to present tasks synchronization.
int important_calculation_semaphore = SEMAPHORE_RELEASED;///<Semaphore used to sychronize tasks that perform important calculation.
//=================================================================================================
//=====================================HEADERS=====================================================
/**
 * @brief Task 1 that kills Task 3 and ends its work.
 */
void Task_1(void);
//=================================================================================================
/**
 * @brief Task 2 that performs important calculation.
 */
void Task_2(void);
//=================================================================================================
/**
 * @brief Task 3 that has to be killed by Task 1
 */
void Task_3(void);
//=================================================================================================
/**
 * @brief Task 4 that performs important calculation.
 */
void Task_4(void);
//=================================================================================================
//=====================================FUNCTIONS===================================================
int main(void)
{
	Scheduler_InitTaskList();
	mythreads_start((void*)main);
	mythreads_start((void*)Task_1);
	mythreads_start((void*)Task_2);
	mythreads_start((void*)Task_3);
	mythreads_start((void*)Task_4);

	Scheduler_ListWriteDown();
	Scheduler_TimerInit(SCHEDULER_TASKS_INTERVAL_US);

	while(1)
	{
		puts("Hello from main!!!\n");
	}

	return EXIT_SUCCESS;
}
//=================================================================================================
void Task_1(void)
{
	mythread_kill((void*)Task_3);


	puts("Killed task 3\n");


	Scheduler_ListWriteDown();
	Scheduler_TimerInit(SCHEDULER_TASKS_INTERVAL_US);

	puts("Hello from task 1\n");

	mythread_exit();
}
//=================================================================================================
void Task_2(void)
{
	int i = 0;

	while(1)
	{
		puts("Hello from task 2\n");
		printf("Task_2 number %d\n", important_calculation);

		while(i < 1000)
		{
			Semaphore_Lift(&important_calculation_semaphore);
			important_calculation++;
			i++;
			Semaphore_Release(&important_calculation_semaphore);
		}


	}
}
//=================================================================================================
void Task_3(void)
{
	while(1)
	{
		puts("Hello from task 3\n");
	}
}
//=================================================================================================
void Task_4(void)
{
	int i = 0;
	while(1)
	{
		puts("Hello from task 4\n");
		printf("Task_4 number %d\n", important_calculation);

		while(i < 1000)
		{
			Semaphore_Lift(&important_calculation_semaphore);
			important_calculation++;
			i++;
			Semaphore_Release(&important_calculation_semaphore);
		}


	}
}
//=====================================EOF=========================================================
