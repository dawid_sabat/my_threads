# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* This is a project to implement on it's own threads in Linux user space.
* The main goal has been to practice writting a clean code.
* The second goal has been to demonstrate a few task that are running pararell in user's sprace.

### How do I get set up? ###

* This project has been set up using Eclipse Neon and Ubuntu LTS 16.04.

### Main features. ###

* There can be created as many tasks as user's computer memory allows.
* All task pointers and contexts are stored in two-way ciclic list.
* Timer allows to perform interrupt from scheduler and save current task context and switch it to the next on the tasks list.
* Working tasks can be killed or can end theirs work.
* To sychronize some calculations that are shared by a couple of tasks, semaphores were implemented.

### How demo works? ###

* Four (five including main task) tasks are created at the beginnign of the program.
* The goal of Task 1 is to kill Task 3 and end its work.
* The goal of Task 2 and 4 is to say hello and perform some common varialbe incrementation.
* The goal of Task 3 is to be killed by Task 1.
* The goal of main task is to initalize all remaining tasks and say hello in loop.
* Scheduler gives 10 us for each task and says hello every time when it is called.

### Contribution guidelines ###

* Any code review is welcomed.

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact